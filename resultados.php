<?php 


include ("complementos/header.php");

$carrera = $_SESSION['carrera'];


?>

<div class="contianer p-5">

	<div class="featured-boxes">
		<div class="row">
			<div class="col-md-12">
				<div class="featured-box featured-box-primary text-left mt-2" style="">
					<div class="box-content">


						<?php 

						if ($carrera == 'industrial') {

							include "vistas/industrial.php";

						}elseif ($carrera == 'sistemas') {

							include "vistas/sistemas.php";
							
						}elseif ($carrera == 'arquitectura') {

							include "vistas/arquitectura.php";

						}elseif ($carrera == 'psicologia') {

							include "vistas/psicologia.php";

						}elseif ($carrera == 'medicina') {

							include "vistas/medicina.php";

						}elseif ($carrera == 'derecho') {

							include "vistas/derecho.php";

						}

						?>

					</div>
					<div class="text-center mt-5">
						<a href="index.php" class="btn btn-primary">Volver a Inicio</a>
					</div>
				</div>
			</div>

		</div>

	</div>
	

</div>


<?php 

include ("complementos/footer.php");

?>