<?php 

$profesion = true;

include ("complementos/header.php");

include "modelos/modeloDatos.php";



 ?>
<div class="contianer p-5">

	<div class="featured-boxes">
 		<div class="row">
 			<div class="col-md-12">
 				<div class="featured-box featured-box-primary text-left mt-2" style="">
 					<div class="box-content">
 						<div class="text-center">
 							<h4  class="color-primary font-weight-semibold text-4 text-uppercase mb-3">!Conoce tu profesion con este Test!</h4>
 						</div>
 						<h5 class="mt-5">Llena todos los datos de este test y conoce tu profesion!</h5>

 						<!-- FORMULARIO -->
 						<form action="controladores/validaciones.php" id="frmSignIn" method="post">

 							<?php 

 							$respuesta = datos::consultarUsuario();

 							foreach ($respuesta as $key => $value) {

 								$id_estudiante = $value;
 								
 							}

 							echo('<input type="hidden" name="id_estudiante" value="'.$id_estudiante.'">');

 							 ?>
 							

 							<div class="form-row mt-2">
 								<div class="form-group col">
 									<label class="font-weight-bold text-dark text-2">1. ¿Eres varón o mujer?</label>
 									<div class="form-check">
 										<input type="hidden" name="pregunta1" value="1" >
 										<input class="form-check-input" type="radio" name="genero"  value="1" required >
 										<label class="form-check-label" for="exampleRadios1" >
 											Hombre
 										</label>
 									</div>
 									<div class="form-check">
 										<input class="form-check-input" type="radio" name="genero"  value="2"  required>
 										<label class="form-check-label" for="exampleRadios1">
 											Mujer
 										</label>
 									</div>
 								</div>
 							</div>
 							<div class="form-row mt-2">
 								<div class="form-group col">
 									<label class="font-weight-bold text-dark text-2">2. En tu tiempo libre prefieres:</label>

 									<div class="form-check">
 										<input type="hidden" name="pregunta2" value="2">
 										<input class="form-check-input" type="radio" name="tiempolibre"  value="1" required >
 										<label class="form-check-label" for="exampleRadios1">
 											Ver películas, escuchar música, cantar en el karaoke, disfrutar del teatro o de la pintura.
 										</label>
 									</div>
 									<div class="form-check">
 										<input class="form-check-input" type="radio" name="tiempolibre"  value="2" required >
 										<label class="form-check-label" for="exampleRadios1">
 											Realizar trabajos de bricolaje, por ejemplo reparaciones electrónicas o jardinería, o practicar deportes, como por ejemplo fútbol, correr, ciclismo o natación.
 										</label>
 									</div>
 								</div>
 							</div>
 							<div class="form-row mt-2">
 								<div class="form-group col">
 									<label class="font-weight-bold text-dark text-2">3. Si pudieras donar una gran suma de dinero, la donarías a:</label>

 									<div class="form-check">
 										<input type="hidden" name="pregunta3" value="3">
 										<input class="form-check-input" type="radio" name="donar"  value="1" required>
 										<label class="form-check-label" for="exampleRadios1">
 											Al equipo de la película de ciencia ficción X-men.
 										</label>
 									</div>
 									<div class="form-check">
 										<input class="form-check-input" type="radio" name="donar" value="2" required>
 										<label class="form-check-label" for="exampleRadios1">
 											ONGs, organizaciones de sensibilización pública y fundaciones que se dedican a la recaudación de fondos, como la fundación WWF.
 										</label>
 									</div>
 								</div>
 							</div>
 							<div class="form-row mt-2">
 								<div class="form-group col">
 									<label class="font-weight-bold text-dark text-2">4. ¿Qué te hace sentirte más realizado?</label>

 									<div class="form-check">
 										<input type="hidden" name="pregunta4" value="4">
 										<input class="form-check-input" type="radio" name="sentir" value="1" required>
 										<label class="form-check-label" for="exampleRadios1">
 											Hacer un buen trabajo por mi mismo
 										</label>
 									</div>
 									<div class="form-check">
 										<input class="form-check-input" type="radio" name="sentir"  value="2" required>
 										<label class="form-check-label" for="exampleRadios1">
 											Dirigir a un grupo para conseguir mis objetivos.
 										</label>
 									</div>
 								</div>
 							</div>
 							<div class="form-row mt-2">
 								<div class="form-group col">
 									<label class="font-weight-bold text-dark text-2">5. En vacaciones prefieres:</label>

 									<div class="form-check">
 										<input type="hidden" name="pregunta5" value="5">
 										<input class="form-check-input" type="radio" name="vacaciones" value="1"  required>
 										<label class="form-check-label" for="exampleRadios1">
 											Hacer escalada o senderismo, viajar por tu cuenta o realizar actividades de aventura al aire libre.
 										</label>
 									</div>
 									<div class="form-check">
 										<input class="form-check-input" type="radio" name="vacaciones"  value="2"  required>
 										<label class="form-check-label" for="exampleRadios1">
 										Quedarte en casa y relajarte, o realizar un viaje organizado por una agencia de viajes.
 										</label>
 									</div>
 								</div>
 							</div>
 							<div class="form-row mt-2">
 								<div class="form-group col">
 									<label class="font-weight-bold text-dark text-2">6. Tienes mejores aptitudes en:</label>

 									<div class="form-check">
 										<input type="hidden" name="pregunta6" value="6">
 										<input class="form-check-input" type="radio" name="aptitudes" value="1" required >
 										<label class="form-check-label" for="exampleRadios1">La escritura y el arte.
 										</label>
 									</div>
 									<div class="form-check">
 										<input class="form-check-input" type="radio" name="aptitudes" value="2"  required>
 										<label class="form-check-label" for="exampleRadios1">
 										
 										La investigación y el análisis.
 										</label>
 									</div>
 								</div>
 							</div>
 							<div class="form-row mt-2">
 								<div class="form-group col">
 									<label class="font-weight-bold text-dark text-2">7. En las fiestas prefieres:</label>

 									<div class="form-check">
 										<input type="hidden" name="pregunta7" value="7">
 										<input class="form-check-input" type="radio" name="fiestas"  value="1" required>
 										<label class="form-check-label" for="exampleRadios1">Ser espectador y observar cómo se expresan y comportan los demás.
 										</label>
 									</div>
 									<div class="form-check">
 										<input class="form-check-input" type="radio" name="fiestas"  value="2" required>
 										<label class="form-check-label" for="exampleRadios1">
 										Participar y convertirte en el alma de la fiesta.
 										
 										</label>
 									</div>
 								</div>
 							</div>
 							<div class="form-row mt-2">
 								<div class="form-group col">
 									<label class="font-weight-bold text-dark text-2">8. En tu opinión, ¿quién tiene mayor talento?</label>

 									<div class="form-check">
 										<input type="hidden" name="pregunta8" value="8">
 										<input class="form-check-input" type="radio" name="talento"  value="1" required>
 										<label class="form-check-label" for="exampleRadios1">Leonardo Da Vinci - pintor, escultor, músico, arquitecto, ingeniero e inventor.
 										</label>
 									</div>
 									<div class="form-check">
 										<input class="form-check-input" type="radio" name="talento"  value="2" required>
 										<label class="form-check-label" for="exampleRadios1">
 										Bill Gates - creador de Microsoft que ha cambiado las vidas de las personas con las tecnología.
 										</label>
 									</div>
 								</div>
 							</div>
 							<div class="form-row mt-2">
 								<div class="form-group col">
 									<label class="font-weight-bold text-dark text-2">9. Te aporta una gran satisfacción y además eres bueno en:</label>

 									<div class="form-check">
 										<input type="hidden" name="pregunta9" value="9">
 										<input class="form-check-input" type="radio" name="bueno"  value="1" required>
 										<label class="form-check-label" for="exampleRadios1">Pensar, analizar y jugar a juegos de inteligencia.
 										</label>
 									</div>
 									<div class="form-check">
 										<input class="form-check-input" type="radio" name="bueno"  value="2" required>
 										<label class="form-check-label" for="exampleRadios1">
 										Hacer cosas con mis propias manos.
 										</label>
 									</div>
 								</div>
 							</div>
 							<div class="form-row mt-2">
 								<div class="form-group col">
 									<label class="font-weight-bold text-dark text-2">10. ¿Qué programas de televisión prefieres ver?:</label>

 									<div class="form-check">
 										<input type="hidden" name="pregunta10" value="10">
 										<input class="form-check-input" type="radio" name="programas"  value="1" required>
 										<label class="form-check-label" for="exampleRadios1">Documentales del canal Discovery y de la BBC.
 										</label>
 									</div>
 									<div class="form-check">
 										<input class="form-check-input" type="radio" name="programas"  value="2" required>
 										<label class="form-check-label" for="exampleRadios1">
 										Programas sobre finanzas y economía, como "El aprendiz".
 										</label>
 									</div>
 								</div>
 							</div>
 							<div class="form-row">
 								<div class="form-group col-lg-6">
 									<input type="submit" value="enviar" name="enviar" class="btn btn-primary float-right" data-loading-text="Loading...">
 								</div>
 							</div>
 						</form>
 					</div>
 				</div>
 			</div>
 			
 		</div>

 	</div>
	

</div>
	



 <?php 

include ("complementos/footer.php");

  ?>