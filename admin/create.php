<?php

$login = true;
 require_once "modelos/config.php";

$nombre = $celular = $ciudad = $correo = "";
$nombre_err = $celular_err = $ciudad_err = $correo_err = "";
 



if($_SERVER["REQUEST_METHOD"] == "POST"){
    
    // Validar  Nombre
    $input_nombre = trim($_POST["nombre"]);
    if(empty($input_nombre)){
        $nombre_err = "Por favor ingrese el nombre del estudiante.";
    } elseif(!filter_var($input_nombre, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $nombre_err = "Por favor ingrese un nombre válido.";
    } else{
        $nombre = $input_nombre;
    }


    // Validar Celular
    $input_celular = trim($_POST["celular"]);
    if(empty($input_celular)){
        $celular_err = "Por favor ingrese el numero de celular.";     
    } else{
        $celular = $input_celular;
    }
    

    // Validar Ciudad
    $input_ciudad = trim($_POST["ciudad"]);
    if(empty($input_ciudad)){
        $ciudad_err = "Por favor ingrese una ciudad.";     
    } else{
        $ciudad = $input_ciudad;
    }
    

    // Validar correo
    $input_correo = trim($_POST["correo"]);
    if(empty($input_correo)){
        $correo_err = "Por favor ingrese un correo.";     
    } else{
        $correo = $input_correo;
    }
    
  
    if(empty($nombre_err) && empty($celular_err) && empty($ciudad_err) && empty($correo_err)){
     
        $sql = "INSERT INTO estudiantes (ID, Nombre, Celular, Ciudad, Correo) VALUES (NULL, ?, ?, ?, ?)";
         
        if($stmt = mysqli_prepare($link, $sql)){
            
            mysqli_stmt_bind_param($stmt, "ssss", $param_nombre, $param_celular, $param_ciudad, $param_correo );
            
           
            $param_nombre = $nombre;
            $param_celular = $celular;
            $param_ciudad = $ciudad;
            $param_correo = $correo;

            
            
            if(mysqli_stmt_execute($stmt)){
                
                header("location: listado.php");
                exit();
            } else{
                echo "intente mas tarde.";
            }
        }
         
       
        mysqli_stmt_close($stmt);
    }
    
   
    mysqli_close($link);
}

include"complementos/header.php";
?>
 

    <div class="box-shadow">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mt-5">
                        <h2>Agregar Estudiante</h2>
                    </div>
                    <p>Favor diligenciar el siguiente formulario, para agregar un estudiante.</p>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="form-group <?php echo (!empty($nombre_err)) ? 'has-error' : ''; ?>">
                            <label>Nombre</label>
                            <input type="text" name="nombre" class="form-control" value="<?php echo $nombre; ?>">
                            
                        </div>
                        <div class="form-group <?php echo (!empty($celular_err)) ? 'has-error' : ''; ?>">
                            <label>Celular</label>
                            <input type="text" name="celular" class="form-control" value="<?php echo $celular; ?>">
                           
                        </div>
                        <div class="form-group <?php echo (!empty($ciudad_err)) ? 'has-error' : ''; ?>">
                            <label>Ciudad</label>
                            <input type="text" name="ciudad" class="form-control" value="<?php echo $ciudad; ?>">
                           
                        </div>
                        <div class="form-group <?php echo (!empty($correo_err)) ? 'has-error' : ''; ?>">
                            <label>Correo</label>
                            <input type="text" name="correo" class="form-control" value="<?php echo $correo; ?>">
                        
                        </div>
                        <input type="submit" class="btn btn-primary" value="Enviar">
                        <a href="listado.php" class="btn btn-default">Cancelar</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>


<?php 

include "complementos/footer.php";

 ?>
