
<?php 
$login = true;
include "complementos/header.php";


require_once "modelos/config.php";


?>


    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <div class="row mt-5">

                        <div class="col-md-6">
                            <h2 class="pull-left">Estudiantes</h2>
                        </div>

                        <div class="col-md-6 text-right">
                            <a href="create.php" class="btn btn-success pull-right">Agregar nuevo empleado</a>
                        </div>
                        
                    </div>
                   
                   
                    <?php
                    $sql = "SELECT DISTINCT ID, Correo, Nombre, Celular, Ciudad from estudiantes";
                    
                    if($result = mysqli_query($link, $sql)){
                        if(mysqli_num_rows($result) > 0){
                            echo "<table class='table table-bordered table-striped'>";
                            echo "<thead>";
                            echo "<tr>";
                            echo "<th>#</th>";
                            echo "<th>Nombre</th>";
                            echo "<th>Celular</th>";
                            echo "<th>Ciudad</th>";
                            echo "<th>Correo</th>";
                            echo "<th>Acción</th>";
                            echo "</tr>";
                            echo "</thead>";
                            echo "<tbody>";
                            while($row = mysqli_fetch_array($result)){
                                echo "<tr>";
                                echo "<td>" . $row['ID'] . "</td>";
                                echo "<td>" . $row['Nombre'] . "</td>";
                                echo "<td>" . $row['Celular'] . "</td>";
                                echo "<td>" . $row['Ciudad'] . "</td>";
                                echo "<td>" . $row['Correo'] . "</td>";
                                echo "<td>";
                                echo "<a href='read.php?id=". $row['ID'] ."' title='Ver' data-toggle='tooltip'><i class='fas fa-eye'></i></a>";
                                echo "<a href='update.php?id=". $row['ID'] ."' title='Actualizar' data-toggle='tooltip'><i class='fas fa-edit'></i></a>";
                                echo "<a href='delete.php?id=". $row['ID'] ."' title='Borrar' data-toggle='tooltip'><i class='fas fa-trash-alt'></i></a>";
                                echo "</td>";
                                echo "</tr>";
                            }
                            echo "</tbody>";                            
                            echo "</table>";
                         
                            mysqli_free_result($result);
                        } else{
                            echo "<p class='lead'><em>No records were found.</em></p>";
                        }
                    } else{
                        echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
                    }

                   
                    mysqli_close($link);
                    ?>
                </div>
            </div>        
        </div>
    </div>


<?php 

include "complementos/footer.php";
 ?>