<?php
$login = true;
if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
   
   require_once "modelos/config.php";
    
    
    $sql = "SELECT * FROM estudiantes WHERE ID= ?";
    
    if($stmt = mysqli_prepare($link, $sql)){
      
        mysqli_stmt_bind_param($stmt, "i", $param_id);
        
       
        $param_id = trim($_GET["id"]);
        
        
        if(mysqli_stmt_execute($stmt)){
            $result = mysqli_stmt_get_result($stmt);
    
            if(mysqli_num_rows($result) == 1){
               
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                
               
                $nombre = $row["Nombre"];
                $celular = $row["Celular"];
                $ciudad = $row["Ciudad"];
                $correo = $row["Correo"];
            } else{
                
                header("location: error.php");
                exit();
            }
            
        } else{
            echo "Oops! Something went wrong. Please try again later.";
        }
    }
     
   
    mysqli_stmt_close($stmt);
    
   
    mysqli_close($link);
} else{
   
    header("location: error.php");
    exit();
}
include "complementos/header.php";
?>


    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    
                    <h1 class="mt-5">Ver Estudiante</h1>
                    <div class="form-group">
                        <label class="font-weight-bold">Nombre</label>
                        <p class="form-control-static"><?php echo $row["Nombre"]; ?></p>
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold">Celular</label>
                        <p class="form-control-static"><?php echo $row["Celular"]; ?></p>
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold">Ciudad</label>
                        <p class="form-control-static"><?php echo $row["Ciudad"]; ?></p>
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold">Correo</label>
                        <p class="form-control-static"><?php echo $row["Correo"]; ?></p>
                    </div>
                    <p><a href="listado.php" class="btn btn-primary">Volver</a></p>
                </div>
            </div>        
        </div>
    </div>


<?php 

include "complementos/footer.php"
 ?>