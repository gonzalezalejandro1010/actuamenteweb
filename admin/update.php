<?php
$login = true;
require_once "modelos/config.php";
 

$name = $address = $salary = "";
$name_err = $address_err = $salary_err = "";

if(isset($_POST["id"]) && !empty($_POST["id"])){
   
    $id = $_POST["id"];
    
   
    $input_nombre= trim($_POST["nombre"]);
    if(empty($input_nombre)){
        $name_err = "Por favor ingrese un nombre.";
    } elseif(!filter_var($input_nombre, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $name_err = "Por favor ingrese un nombre válido.";
    } else{
        $nombre = $input_nombre;
    }
    

    $input_celular = ($_POST["celular"]);
    if(empty($input_celular)){
        $cel_err = "Por favor ingrese un celular.";     
    } else{
        $celular = $input_celular;
    }


    $input_ciudad = trim($_POST["ciudad"]);
    if(empty($input_ciudad)){
        $ciud_err = "Por favor ingrese una ciudad";     
    } else{
        $ciudad = $input_ciudad;
    }

    $input_correo = trim($_POST["correo"]);
    if(empty($input_correo)){
        $correo_err = "Por favor ingrese un correo";     
    } else{
        $correo = $input_correo;
    }


  
    

    
   
    if(empty($name_err) && empty($cel_err) && empty($ciud_err)  && empty($correo_err)  ){
       
        $sql = "UPDATE estudiantes SET Nombre=?, Celular=?, Ciudad=?, Correo=? WHERE ID=?";
         
        if($stmt = mysqli_prepare($link, $sql)){
            
            mysqli_stmt_bind_param($stmt, "ssssi", $param_nombre, $param_celular, $param_ciudad, $param_correo,  $param_id);
            
            
            $param_nombre = $nombre;
            $param_celular = $celular;
            $param_ciudad = $ciudad;
            $param_correo = $correo;
          
            $param_id = $id;
            
            
            if(mysqli_stmt_execute($stmt)){
              
                header("location: listado.php");
                exit();
            } else{
                echo "Intente mas tarde.";
            }
        }
         
       
        mysqli_stmt_close($stmt);
    }
    
   
    mysqli_close($link);
} else{
   
    if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
       
        $id =  trim($_GET["id"]);
        
        
        $sql = "SELECT * FROM estudiantes WHERE ID = ?";
        if($stmt = mysqli_prepare($link, $sql)){
            
            mysqli_stmt_bind_param($stmt, "i", $param_id);
            
          
            $param_id = $id;
            
            
            if(mysqli_stmt_execute($stmt)){
                $result = mysqli_stmt_get_result($stmt);
    
                if(mysqli_num_rows($result) == 1){
                   
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    
                    
                    $nombre = $row["Nombre"];
                    $celular = $row["Celular"];
                    $ciudad = $row["Ciudad"];
                    $correo = $row["Correo"];
                   
                } else{
                    
                    header("location: error.php");
                    exit();
                }
                
            } else{
                echo "intente despues";
            }
        }
        
       
        mysqli_stmt_close($stmt);
        
       
        mysqli_close($link);
    }  else{
       
        header("location: error.php");
        exit();
    }
}

include "complementos/header.php";
?>
 
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    
                    <h2 class="mt-5">Actualizar Registro</h2>
                  
                    <p>Edite los valores de entrada y envíe para actualizar el registro.</p>
                    <form action="<?php echo htmlspecialchars(basename($_SERVER['REQUEST_URI'])); ?>" method="post">
                        <div class="form-group <?php echo (!empty($name_err)) ? 'has-error' : ''; ?>">
                            <label class="font-weight-bold">Nombre</label>
                            <input type="text" name="nombre" class="form-control" value="<?php echo $nombre; ?>">
                          
                        </div>
                        <div class="form-group <?php echo (!empty($cel_err)) ? 'has-error' : ''; ?>">
                            <label class="font-weight-bold">Celular</label>
                            <input type="text" name="celular" class="form-control" value="<?php echo $celular; ?>">
                           
                        </div>
                        <div class="form-group <?php echo (!empty($ciud_err)) ? 'has-error' : ''; ?>">
                            <label class="font-weight-bold">Ciudad</label>
                             <input type="text" name="ciudad" class="form-control" value="<?php echo $ciudad; ?>">
                           
                        </div>
                        <div class="form-group <?php echo (!empty($correo_err)) ? 'has-error' : ''; ?>">
                            <label class="font-weight-bold">Correo</label>
                            <input type="text" name="correo" class="form-control" value="<?php echo $correo; ?>">
                           
                        </div>
                      
                        <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                        <input type="submit" class="btn btn-primary" value="Enviar">
                        <a href="listado.php" class="btn btn-default">Cancelar</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>


<?php 

include "complementos/footer.php";

 ?>