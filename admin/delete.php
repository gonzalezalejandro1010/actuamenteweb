<?php
$login = true;


if(isset($_POST["id"]) && !empty($_POST["id"])){
  
   require_once "modelos/config.php";
    
 
    $sql = "DELETE FROM estudiantes WHERE ID = ?";
    
    if($stmt = mysqli_prepare($link, $sql)){
       
        mysqli_stmt_bind_param($stmt, "i", $param_id);
        
      
        $param_id = trim($_POST["id"]);
        
        
        if(mysqli_stmt_execute($stmt)){
         
            header("location: listado.php");
            exit();
        } else{
            echo "Intente mas tarde.";
        }
    }
     
 
    mysqli_stmt_close($stmt);
    
   
    mysqli_close($link);
} else{
   
    if(empty(trim($_GET["id"]))){
       
        header("location: error.php");
        exit();
    }
}
include "complementos/header.php";
?>

    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    
                    <h1 class="mt-5">Borrar Registro</h1>
                  
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="alert alert-danger" role="alert">
                            <input type="hidden" name="id" value="<?php echo trim($_GET["id"]); ?>"/>
                            <p>Está seguro que deseas borrar el registro</p><br>
                            <p>
                                <input type="submit" value="Si" class="btn btn-danger">
                                <a href="listado.php" class="btn btn-default">No</a>
                            </p>
                        </div>
                    </form>
                </div>
            </div>        
        </div>
    </div>


<?php 

include "complementos/footer.php";

 ?>