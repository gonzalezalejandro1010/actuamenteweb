
<?php 

$login = true;

include"complementos/header.php";

 ?>

 <section class="container mt-5">
 	<div class="row">
 		<div class="col-md-12">
 			<div class="featured-box featured-box-primary text-left mt-2" style="height: 357.52px;">
 				<div class="box-content">
 					<h4 class="color-primary font-weight-semibold text-4 text-uppercase mb-3">INICIAR SESION ADMIN - ACTUAMENTE</h4>
 					<form action="../controladores/validacionusuario.php" id="frmSignIn" method="post">
 						<div class="form-row">
 							<div class="form-group col">
 								<label class="font-weight-bold text-dark text-2">Correo</label>
 								<input type="text" value="" name="correo" class="form-control form-control-lg">
 							</div>
 						</div>
 						<div class="form-row">
 							<div class="form-group col">
 							
 								<label class="font-weight-bold text-dark text-2">Contraseña</label>
 								<input type="password" value="" name="contra" class="form-control form-control-lg">
 							</div>
 						</div>
 						<div class="form-row">
 							
 							<div class="form-group col-lg-6">
 								<input type="submit" value="Enviar" class="btn btn-primary float-right" data-loading-text="Loading...">
 							</div>
 						</div>
 					</form>
 				</div>
 			</div>
 		</div>
 	</div>

 </section>


<?php 
include"../complementos/footer.php";

 ?>
