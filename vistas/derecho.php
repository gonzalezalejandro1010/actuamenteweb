 <div class="text-center">
 	<h4 class="color-primary font-weight-semibold text-4 text-uppercase mb-3">!Gracias por enviar tu test!</h4>
 </div>

 <p class="mt-5 text-dark">Teniendo en cuenta todos los campos de este formulario y tu forma de pensar y ver la vida, nos agrada decirte que calificarias a la carrera <span class="text-primary">DERECHO!</span></p>

 <p class="text-dark">Conoce un poco mas de esta carrera! </p>

 <div class="container mt-5">

 	<div class="row">

 		<div class="col-md-6">

 			<div class="embed-responsive embed-responsive-16by9">
 				<iframe width="560" height="315" src="https://www.youtube.com/embed/x_lvYiXBkns" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
 			</div>

 		</div>

 		<div class="col-md-6">

 			<h3>Derecho</h3>
 			<p class="mt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
 				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
 				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
 			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

 		</div>


 	</div>

 </div>    