<!-- ----------MODO CELULAR----------------- -->

<div class="slider-with-overlay letradoble">
	<div class="slider-container rev_slider_wrapper" style="height: 670px;">
		<div id="revolutionSlider" class="slider rev_slider" data-version="5.4.8" data-plugin-revolution-slider data-plugin-options="{'sliderLayout': 'fullscreen', 'delay': 9000, 'gridwidth': 1140, 'gridheight': 800, 'responsiveLevels': [4096,1200,992,500]}">
			
			<ul>
				<li class="slide-overlay slide-overlay-level-6" data-transition="fade">
					<div class="tp-caption"
					data-x="center" 
					data-y="center" data-voffset="['-190','60','60','-250']"
					data-start="1000"
					data-transform_in="x:[-300%]/*;opacity:0;s:500;"
					data-transform_idle=""><img src="images/iconobombillo200.png" alt="" style="width: 100%"></div>
					<img src="images/fondo.jpg"  
					alt="banner-1000razones"
					data-bgposition="center center" 
					data-bgfit="cover" 
					data-bgrepeat="no-repeat" 
					class="rev-slidebg">

					<div class="tp-caption"
					data-x="center" data-hoffset="['-170','-170','-170','-350']"
					data-y="center" data-voffset="['-50','-50','-50','-75']"
					data-start="1000"
					data-transform_in="x:[-300%];opacity:0;s:500;"
					data-transform_idle="opacity:0.2;s:500;"><img src="img/slides/slide-title-border.png" alt="" style=""></div>

					<div class="tp-caption text-color-light font-weight-normal"
					data-x="center"
					data-y="center" data-voffset="['-50','-50','-50','-75']"
					data-start="700"
					data-fontsize="['16','16','16','40']"
					data-lineheight="['32','32','32','45']"
					data-transform_in="y:[-50%];opacity:0;s:500;">CAMBIA TU VIDA</div>

					<div class="tp-caption"
					data-x="center" data-hoffset="['170','170','170','350']"
					data-y="center" data-voffset="['-50','-50','-50','-75']"
					data-start="1000"
					data-transform_in="x:[300%];opacity:0;s:500;"
					data-transform_idle="opacity:0.2;s:500;"><img src="img/slides/slide-title-border.png" alt=""></div>

					<div id="menu" class="tp-caption font-weight-extra-bold text-color-light negative-ls-1" 
					data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:1.5;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
					data-x="center"
					data-y="center"
					data-fontsize="['60','60','60','90']"
					data-lineheight="['55','55','55','95']"
					style="font-family: dulce !important; color: #DB9600 !important;">1000 Razones</div>

					<div class="tp-caption font-weight-light ws-normal text-center"
					data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":2000,"split":"chars","splitdelay":0.05,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
					data-x="center"
					data-y="center" data-voffset="['70','60','60','105']"
					data-width="['530','530','530','1100']"
					data-fontsize="['18','18','18','40']"
					data-lineheight="['19','19','19','45']"
					style="color: white;">Mas de 30,000 razones te ayudaran a tener 1000 razones para tu vida.</div>

					<a class="ancla tp-caption btn btn-primary btn-outline font-weight-bold border-width-4 text-color-light"
					href="#razones"
					data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"y:50%;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
					data-x="center"
					data-y="center" data-voffset="['160','130','130','280']"
					data-paddingtop="['18','18','18','30']"
					data-paddingbottom="['18','18','18','30']"
					data-paddingleft="['40','40','40','80']"
					data-paddingright="['40','40','40','80']"
					data-fontsize="['35','35','35','55']"
					data-lineheight="['18','18','18','55']" style="font-family: dulce !important; color: white !important; ">¡ Déjanos tu razon !</a>
					
				</li>
			</ul>
		</div>
	</div>

	<div class="slider-contact-form" style="">
		<div class="container">
			<div class="row justify-content-center">
				<form id="register_form" class="contact-form p-4 rounded bg-color-grey" >	
					<div class="p-4">
						<form method="post">
							<h4 class="text-center" style=" font-size: 20px !important;">¡Ingresa tus datos!</h4>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="name" style="color: white !important; font-size: 15px !important;">Nombre</label>
										<input type="text" class="form-control" name="name" id="name" placeholder="Tu nombre">
									</div>
									
									
									<div class="form-group">
										<label for="name" style="color: white !important; font-size: 15px !important;">Celular</label>
										<input type="number" class="form-control" name="celular" id="celular" placeholder="Tu celular">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="name"  style="color: white !important; font-size: 15px !important;">Correo</label>
										<input type="mail" class="form-control" name="email" id="email" placeholder="Tu Correo" >
									</div>
									<div class="form-group ">
										<label for="comment"  style="color: white !important; font-size: 15px !important;">Ciudad</label>
										<input type="text" class="form-control" name="ciudad" id="ciudad" placeholder="ciudad">
									</div>

								</div>
								

							</div>
							
							<button type="submit" class="btn btn-primary" name="btnEnviar" value="Enviar" style="color: white !important; font-size: 15px !important;">Enviar :)</button>
						</form>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>