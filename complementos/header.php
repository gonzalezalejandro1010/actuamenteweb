<!DOCTYPE html>
<html>
<head>
	<?php 
	

	session_start();
	
	?>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<title>ActuaMente</title>	

	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="okler.net">

	<!-- Favicon -->
	<link rel="shortcut icon" href="imagenes/logo.png" type="favicon" />
	<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

	<!-- Web Fonts  -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light%7CPlayfair+Display:400" rel="stylesheet" type="text/css">
	<style type="text/css">
		@font-face{
			font-family: dulce;
			src: url(Dulcelin.otf);
		}


	</style>

	<!-- Vendor CSS -->
<link rel="stylesheet" href="vendor/sweetalert2/dist/sweetalert2.min.css">
	<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="vendor/fontawesome-free/css/all.min.css">
	<link rel="stylesheet" href="vendor/animate/animate.min.css">
	<link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
	<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
	<link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">
	<link rel="stylesheet" href="vendor/circle-flip-slideshow/css/component.css">

	<!-- Theme CSS -->

	<link rel="stylesheet" href="css/theme.css">
	<link rel="stylesheet" href="css/theme-elements.css">
	<link rel="stylesheet" href="css/theme-blog.css">
	<link rel="stylesheet" href="css/theme-shop.css">

	<!-- Current Page CSS -->
	<link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
	<link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">

	<!-- Demo CSS -->


	<!-- Skin CSS -->
	<link rel="stylesheet" href="css/skins/default.css"> 

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="css/custom.css">

	<!-- Head Libs -->
	<script src="vendor/modernizr/modernizr.min.js"></script>

</head>
<body>

	<div class="body">
	   
		<style>
			.opacity{
				background: white;
				opacity:0.9; /* Opacidad 60% */
				width:200px;
				height:70px;
			}
		</style>

		
		<header id="header" class=" " data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 80}">

			<div class="header-body border-top-0 bg-dark box-shadow-none" data-appear-animation="customHeaderAnimOne" style="border-bottom: 2px solid #DB9600 !important;">
				<div class="header-container header-container-height-sm container">
					<div class="header-row">
						<div class="header-column  flex-grow-0 ">
							<div class="header-row pr-2">
								<div class="header-logo">
									<a href="index.php">
										<img alt="" width="80" height="80" src="imagenes/logo.png">
									</a>
								</div>
							</div>
						</div>
						<style type="text/css">
							.hola{
								margin-left: 25px !important;
							}
						</style>
						<div class="header-column">
							<div class="header-row ">
								<div class="header-nav header-nav-links header-nav-dropdowns-dark header-nav-light-text order-2 order-lg-1 justify-content-center">
									<div class="header-nav-main header-nav-main-mobile-dark header-nav-main-square  header-nav-main-effect-4 header-nav-main-sub-effect-6 text-center">
										<nav class="collapse">
											<ul class="nav nav-pills" id="mainNav">
												<li class="dropdown">
													<a class="dropdown-item dropdown-toggle <?php if(isset($inicio)){ echo 'active';} ?>" href="index.php">
														Inicio
													</a>
												</li>
												
												<li class="dropdown">
													<a class="dropdown-item dropdown-toggle <?php if(isset($carreras)){ echo 'active';} ?>" href="carreras.php">
														Carreras
													</a>
												</li>
											</ul>
										</nav>
									</div>
								</div>
							</div>
						</div>
						<div class="header-column flex-grow-0 justify-content-end">
							<div class="header-row justify-content-center">
								<ul class="header-social-icons social-icons d-none d-sm-block social-icons-clean m-0">
									<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
									<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
									<li class="social-icons-instagram"><a href="http://www.instagram.com/" target="_blank" title="Instagram"><i class="fab fa-instagram"></i></a></li>
								</ul>
								<ul class="nav nav-pills" id="mainNav">
									<li class="dropdown">
										<a class="dropdown-item  <?php if(isset($login)){ echo 'active';} ?>" href="admin/login.php">
											Administrador
										</a>
									</li>
								</ul>
								
								
								<button class="btn header-btn-collapse-nav ml-0 ml-sm-3" data-toggle="collapse" data-target=".header-nav-main nav">
									<i class="fas fa-bars"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
<div role="main" class="main">
