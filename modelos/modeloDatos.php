<?php 


class datos{

	static function insertardatos($nombre,$correo,$celular,$ciudad){

		include 'conexion.php';

		$stmt=$pdo->prepare("INSERT INTO estudiantes (ID, Nombre, Celular, Ciudad, Correo) VALUES (NULL ,:nombre, :celular, :ciudad, :correo)");

		$stmt->bindParam(":nombre", $nombre, PDO::PARAM_STR);
		$stmt->bindParam(":celular", $celular, PDO::PARAM_INT);	
		$stmt->bindParam(":ciudad", $ciudad, PDO::PARAM_STR);	
		$stmt->bindParam(":correo", $correo, PDO::PARAM_STR);	

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt-> close();

		$stmt = null;


	}

	/*CONSULTA ADMIN*/

	static function buscarusuario($correo,$pass){

		include 'conexion.php';

		$stmt=$pdo->prepare("SELECT * FROM admin WHERE correo = :correo AND pass = :pass");

		$stmt->bindParam(":pass", $pass, PDO::PARAM_STR);
		$stmt->bindParam(":correo", $correo, PDO::PARAM_STR);

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$tmt =null;

	}


	/*INSERTAR RESPUESTAS*/

	static function insertardatostest($arraypreguntas,$arrayrespuestas,$id_estudiante){

		include 'conexion.php';

		$stmt=$pdo->prepare("INSERT INTO `resultado` (`id`, `id_estudiante`, `id_pregunta`, `id_repuesta`) VALUES (NULL, :estudiante, :pregunta, :respuesta)");

		$stmt->bindParam(":pregunta", $arraypreguntas, PDO::PARAM_INT);
		$stmt->bindParam(":respuesta", $arrayrespuestas, PDO::PARAM_INT);
		$stmt->bindParam(":estudiante", $id_estudiante, PDO::PARAM_INT);		
	

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt-> close();

		$stmt = null;


	}


	/*CONSULTAR ESTUDIANTE*/

	 public function consultarUsuario(){

	 	include 'conexion.php';

		$stmt=$pdo->prepare("SELECT MAX(id) AS ID FROM estudiantes");

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$tmt =null;

	}


}


