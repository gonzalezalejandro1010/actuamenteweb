<?php 

$carreras = true;

include ("complementos/header.php");

 ?>


 <div class="container">

<div class="text-center mt-5">

	<h3 class="text-primary">Carreras</h3>
	
</div>


 	<section class="container mt-5">

 		<div class="row">

 			<div class="col-md-6">

 				<h3>Derecho</h3>
 				<p class="mt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
 				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
 				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
 				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
 				
 			</div>
 			
 			<div class="col-md-6">

 				<div class="embed-responsive embed-responsive-16by9">
 					<iframe width="560" height="315" src="https://www.youtube.com/embed/x_lvYiXBkns" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
 				</div>

 			</div>

 		</div>
 		
 	</section>

 	<section class="container mt-5">

 		<div class="row">

 			<div class="col-md-6">

 				<div class="embed-responsive embed-responsive-16by9">
 					<iframe width="560" height="315" src="https://www.youtube.com/embed/naVEXdC6ce8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
 				</div>

 			</div>

 			<div class="col-md-6">

 				<h3>Ingeniria Industrial</h3>
 				<p class="mt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
 				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
 				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
 				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
 				
 			</div>
 			

 		</div>
 		
 	</section>

 	<section class="container mt-5">

 		<div class="row">

 			<div class="col-md-6">

 				<h3>Medicina</h3>
 				<p class="mt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
 				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
 				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
 				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
 				
 			</div>
 			
 			<div class="col-md-6">

 				<div class="embed-responsive embed-responsive-16by9">
 					<iframe width="560" height="315" src="https://www.youtube.com/embed/LwVSNnMuGRM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
 				</div>

 			</div>

 		</div>
 		
 	</section>

 	<section class="container mt-5">

 		<div class="row">

 			<div class="col-md-6">

 				<div class="embed-responsive embed-responsive-16by9">
 					<iframe width="560" height="315" src="https://www.youtube.com/embed/mE9gPU-3XbI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
 				</div>

 			</div>

 			<div class="col-md-6">

 				<h3>Psicologia</h3>
 				<p class="mt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
 				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
 				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
 				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
 				
 			</div>
 			

 		</div>
 		
 	</section>

 	<section class="container mt-5">

 		<div class="row">

 			<div class="col-md-6">

 				<h3>Arquitectura</h3>
 				<p class="mt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
 				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
 				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
 				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
 				
 			</div>
 			
 			<div class="col-md-6">

 				<div class="embed-responsive embed-responsive-16by9">
 					<iframe width="560" height="315" src="https://www.youtube.com/embed/RBrVHrelWcY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
 				</div>

 			</div>

 		</div>
 		
 	</section>

 	<section class="container mt-5">

 		<div class="row">

 			<div class="col-md-6">

 				<div class="embed-responsive embed-responsive-16by9">
 					<iframe width="560" height="315" src="https://www.youtube.com/embed/ZYmIUiK8ZQI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
 				</div>

 			</div>

 			<div class="col-md-6">

 				<h3>Ingenieria de sistemas</h3>
 				<p class="mt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
 				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
 				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
 				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
 				
 			</div>
 			

 		</div>
 		
 	</section>










 	

 	<div class="text-center mt-5">
 		<a href="index.php" class="btn btn-primary">Quiero saber mi profesion</a>
 	</div>


 </div>




 <?php 

include ("complementos/footer.php");

  ?>